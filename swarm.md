![](imagens/001.png)

![](imagens/002.png)

# habilitar o Swarm
```
docker swarm init --advertise-addr “ip_do_server”
```
#### Para solicitar tokens Manager e Work
```
docker swarm join-token manager
docker swarm join-token worker
```

#### Listar nodes
```
docker node docker node ls
```

# Visualizer
```
docker run -dit \
-p 8080:8080 \
-v /var/run/docker.sock:/var/run/docker.sock \
dockersamples/visualizer
```

# Servicos

#### Criar um serviço escalável
```
docker service create \
--replicas 1 \
--name replicado -p 4000:80 \
nginx
```
#### Tratando o serviço
Listando os serviços
```
docker service ls
```
Escalando para MAIS containers
```
docker service scale new_service=3
```

Escalando para MENOS containers
```
docker service scale new_service=1
```

#### Criar um serviço Global
```
docker service create \
--mode global \
--name global \
-p 4001:80 \
rayeshuang/friendlyhello
```

# Criando um serviço pelo stack

Criar os diretórios para persistir os dados:

```
mkdir /srv/bd
mkdir /srv/wordpress
```

Criar o stack.yml

```
version: '3.1'

services:
  db:
    image: mysql:5.7
    restart: always
    deploy:
      replicas: 1
      #mode: global 
    environment:
      MYSQL_DATABASE: exampledb
      MYSQL_USER: exampleuser
      MYSQL_PASSWORD: examplepass
      MYSQL_RANDOM_ROOT_PASSWORD: '1'
#    volumes:
#      - /srv/bd:/var/lib/mysql

  wordpress:
    image: wordpress
    restart: always
    deploy:
      replicas: 1   
    ports:
      - 4003:80
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: exampleuser
      WORDPRESS_DB_PASSWORD: examplepass
      WORDPRESS_DB_NAME: exampledb
#    volumes:
#      - /srv/wordpress:/var/www/html
```

# Comandos para verificar o stack

Criar o stack
```
docker stack deploy -c stack.yml wordpress
```
```
docker stack ps wordpress
```
```
docker service ps wordpress_wordpress
```
```
docker service scale wordpress_wordpress=2
```

**INFO**: Como não está persistindo dados, vamos decrementar somente para 1 container.
```
docker service scale wordpress_wordpress=1
```

Fonte: https://hub.docker.com/_/wordpress/
